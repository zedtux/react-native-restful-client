import RestfulClient from '..'

describe('RestfulClient', () => {
  describe('constructor', () => {
    it('should throw an error when not passing the baseUrl', () => {
      expect(() => {
        new RestfulClient()
      }).toThrow('missing baseUrl')
    })

    it('should throw an error when not passing the resource', () => {
      expect(() => {
        new RestfulClient('https://api.domain.co')
      }).toThrow('missing resource')
    })

    it('should use the current origin when passing a slash as baseUrl', () => {
      expect(new RestfulClient('/', { resource: 'users' }).baseUrl).toEqual(
        'https://api.domain.co'
      )
    })

    describe('with mininal valid setup', () => {
      let client

      beforeAll(() => {
        client = new RestfulClient('https://api.domain.co', {
          resource: 'users'
        })
      })

      it('should initialize the internal headers', () => {
        expect(client.headers).toEqual({
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        })
      })

      it('should initialize the internal baseUrl', () => {
        expect(client.baseUrl).toEqual('https://api.domain.co')
      })

      it('should initialize the internal baseUrl', () => {
        expect(client.resource).toEqual('users')
      })
    })
  })
})
