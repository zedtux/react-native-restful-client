import fetchMock from 'fetch-mock'

import RestfulClient from '..'

// ~~~~ Config ~~~~
const API_URL = 'https://api.domain.co/api'

let sessionsApi
let sessionsApiFetch
let sessionsApiPromise

describe('RestfulClient Storage', () => {
  beforeAll(() => {
    // Mocks the session creation request
    fetchMock.post(`${API_URL}/sessions`, {
      body: JSON.stringify({
        access_token: 'this-is-your-access_token',
        refresh_token: 'this-is-your-refresh_token'
      }),
      status: 201
    })

    class SessionsApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'sessions' })
      }
    }

    sessionsApi = new SessionsApi()
    sessionsApiFetch = jest.spyOn(sessionsApi, '_fetch')
  })

  beforeEach(() => {
    localStorage.clear()
    sessionStorage.clear()
  })

  afterAll(() => {
    sessionsApiFetch.mockRestore()
  })

  describe('default', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    it('should store the access_token in the localStorage', () => {
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the localStorage', () => {
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })

  describe('configuring storage to `session`', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        storage: 'session',
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('should store the access_token in the storage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the storage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })

  describe('configuring storage as an object setting accessToken to "session" only', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        storage: {
          accessToken: 'session'
        },
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('should store the access_token in the sessionStorage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the localStorage', () => {
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })

  describe('configuring storage as an object setting refreshToken to "session" only', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        storage: {
          refreshToken: 'session'
        },
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('should store the access_token in the localStorage', () => {
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the sessionStorage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })

  describe('configuring storage as an object setting both tokens to "session"', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        storage: {
          accessToken: 'session',
          refreshToken: 'session'
        },
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('should store the access_token in the sessionStorage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the sessionStorage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })

  describe('configuring storage as an object setting accessToken to "session" and refreshToken to "local"', () => {
    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        storage: {
          accessToken: 'session',
          refreshToken: 'local'
        },
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('should store the access_token in the sessionStorage', () => {
      sessionsApiPromise.then(() => (
        expect(sessionStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-access_token'
        )
      ))
    })

    it('should store the refresh_token in the localStorage', () => {
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-refresh_token'
        )
      ))
    })
  })
})
