import fetchMock from 'fetch-mock'

import RestfulClient from '..'

// ~~~~ Config ~~~~
const API_URL = 'https://api.domain.co/api'

const POSTS = [
  {
    id: 1,
    title: 'First post',
    body: 'This is the first post.'
  },
  {
    id: 2,
    title: 'A brand new way of communicating',
    body: 'In this post we will see that ...'
  }
]

// ~~~~ Global Variables ~~~~
let sessionsApi
let sessionsApiFetch
let sessionsApiPromise
let postsApi
let postsApiFetch
let postsApiPromise
let tryRenewJwt
let renewCallback

describe('RestfulClient token autorenew', () => {
  beforeAll(() => {
    // Session creation API with a token in the root's object response
    class SessionsApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'sessions' })
      }
    }
    sessionsApi = new SessionsApi()
    sessionsApiFetch = jest.spyOn(sessionsApi, '_fetch')

    // Get posts with JWT from session
    class PostsApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'posts' })
      }
    }
    postsApi = new PostsApi()
    postsApiFetch = jest.spyOn(postsApi, '_fetch')
  })

  afterAll(() => {
    sessionsApiFetch.mockRestore()
    postsApiFetch.mockRestore()
  })

  describe('default token renewing', () => {
    beforeAll(() => {
      // Mocks the session creation request
      fetchMock.post(`${API_URL}/sessions`, {
        body: JSON.stringify({
          token: 'this-is-your-initial-token'
        }),
        status: 201
      })

      // Mocks the posts fetching request acting as an unauthenticated request
      fetchMock.get(`${API_URL}/posts`, {
        body: JSON.stringify({}),
        status: 401
      })

      tryRenewJwt = jest.spyOn(postsApi, '_tryRenewJwt')
    })

    afterAll(() => {
      fetchMock.reset()
      tryRenewJwt.mockRestore()
    })

    beforeEach(() => {
      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('stores the initial token in the localStorage', () => (
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-initial-token'
        )
      ))
    ))

    describe('without a configured renewPath', () => {
      describe('when the token has expired', () => {
        it('should not call the renew path', () => {
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(tryRenewJwt).not.toHaveBeenCalled()
          })
        })

        it('should have not updated the stored token from the localStorage', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(localStorage.getItem('restfulclient:jwt')).toEqual(
              'this-is-your-initial-token'
            )
          })
        ))
      })
    })

    describe('with a configured renewPath', () => {
      describe('and without a callback function', () => {
        beforeAll(() => {
          // Mocks the JWT renew request
          fetchMock.post(`${API_URL}/renew`, {
            body: JSON.stringify({
              token: 'this-is-your-renewed-token'
            }),
            status: 201
          }, {
            overwriteRoutes: true
          })
        })

        beforeEach(() => {
          RestfulClient.configure({
            tokenRenewPath: `${API_URL}/renew`
          })
        })

        describe('when the token has expired', () => {
          it('should call the renew path', () => (
            sessionsApiPromise.then(() => {
              postsApiPromise = postsApi.all()

              return postsApiPromise.then(response => (
                expect(tryRenewJwt).toHaveBeenCalled()
              ))
            })
          ))

          it('should send the token to renew it', () => (
            sessionsApiPromise.then(() => (
              postsApi.all().then(response => (
                expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/renew`, {
                  body: JSON.stringify({
                    token: 'this-is-your-initial-token'
                  }),
                  headers: {
                    "Accept": "application/json",
                    "Content-Type": "application/json"
                  },
                  method: "POST"
                })
              ))
            ))
          ))

          it('should have updated the stored token from the localStorage', () => (
            sessionsApiPromise.then(() => {
              postsApiPromise = postsApi.all()

              return postsApiPromise.then(response => (
                expect(localStorage.getItem('restfulclient:jwt')).toEqual(
                  'this-is-your-renewed-token'
                )
              ))
            })
          ))
        })
      })

      describe('and with a callback function', () => {
        beforeAll(() => {
          renewCallback = jest.fn()
        })

        beforeEach(() => {
          RestfulClient.configure({
            tokenRenewPath: `${API_URL}/renew`,
            tokenRenewCallback: renewCallback
          })
        })

        describe('when then token has expired', () => {
          it('should call the renew path', () => {
            sessionsApiPromise.then(() => {
              postsApiPromise = postsApi.all()

              return postsApiPromise.then(response => (
                expect(tryRenewJwt).toHaveBeenCalled()
              ))
            })
          })

          describe('when the server return a new token', () => {
            beforeAll(() => {
              // Mocks the JWT renew request
              fetchMock.post(`${API_URL}/renew`, {
                body: JSON.stringify({
                  token: 'this-is-your-renewed-token'
                }),
                status: 201
              }, {
                overwriteRoutes: true
              })
            })

            it('should updates the stored token from the localStorage', () => (
              sessionsApiPromise.then(() => {
                postsApiPromise = postsApi.all()

                return postsApiPromise.then(response => (
                  expect(localStorage.getItem('restfulclient:jwt')).toEqual(
                    'this-is-your-renewed-token'
                  )
                ))
              })
            ))

            it('should triggers the renew callback', () => (
              sessionsApiPromise.then(() => {
                postsApiPromise = postsApi.all()

                return postsApiPromise.then(response => (
                  expect(renewCallback).toHaveBeenCalled()
                ))
              })
            ))
          })

          describe('when the server did not returned a new token', () => {
            beforeAll(() => {
              // Mocks the JWT renew request
              fetchMock.post(`${API_URL}/renew`, {
                body: {},
                status: 422
              }, {
                overwriteRoutes: true
              })
            })

            it('should not update the stored token from the localStorage', () => (
              sessionsApiPromise.then(() => {
                postsApiPromise = postsApi.all()

                return postsApiPromise.then(response => (
                  expect(localStorage.getItem('restfulclient:jwt')).toEqual(
                    'this-is-your-initial-token'
                  )
                ))
              })
            ))

            it('should not trigger the renew callback', () => (
              sessionsApiPromise.then(() => {
                postsApiPromise = postsApi.all()

                return postsApiPromise.then(response => (
                  expect(renewCallback).not.toHaveBeenCalled()
                ))
              })
            ))
          })
        })
      })
    })
  })

  describe('token renewing with a configured token name', () => {
    beforeAll(() => {
      // Mocks the session creation request
      fetchMock.post(`${API_URL}/sessions`, {
        body: JSON.stringify({
          access_token: 'this-is-your-initial-token'
        }),
        status: 201
      })

      fetchMock.post(`${API_URL}/renew`, {
        body: JSON.stringify({
          access_token: 'this-is-your-renewed-access_token'
        }),
        status: 201
      }, {
        overwriteRoutes: true
      })

      // Mocks the posts fetching request acting as an unauthenticated request
      fetchMock.get(`${API_URL}/posts`, {
        body: JSON.stringify({}),
        status: 401
      })

      tryRenewJwt = jest.spyOn(postsApi, '_tryRenewJwt')
    })

    afterAll(() => {
      fetchMock.reset()
      tryRenewJwt.mockRestore()
    })

    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        tokenRenewPath: `${API_URL}/renew`
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('stores the initial token in the localStorage', () => (
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-initial-token'
        )
      ))
    ))

    describe('when the token has expired', () => {
      it('should call the renew path', () => (
        sessionsApiPromise.then(() => {
          postsApiPromise = postsApi.all()

          return postsApiPromise.then(response => (
            expect(tryRenewJwt).toHaveBeenCalled()
          ))
        })
      ))

      it('should send the token as `access_token` to renew it', () => (
        sessionsApiPromise.then(() => (
          postsApi.all().then(response => (
            expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/renew`, {
              body: JSON.stringify({
                access_token: 'this-is-your-initial-token'
              }),
              headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
              },
              method: "POST"
            })
          ))
        ))
      ))

      it('should have updated the stored token from the localStorage', () => (
        sessionsApiPromise.then(() => {
          postsApiPromise = postsApi.all()

          return postsApiPromise.then(response => (
            expect(localStorage.getItem('restfulclient:jwt')).toEqual(
              'this-is-your-renewed-access_token'
            )
          ))
        })
      ))
    })
  })

  describe('token renewing with refresh token enabled', () => {
    beforeAll(() => {
      // Mocks the session creation request
      fetchMock.post(`${API_URL}/sessions`, {
        body: JSON.stringify({
          access_token: 'this-is-your-initial-access_token',
          refresh_token: 'this-is-your-initial-refresh_token'
        }),
        status: 201
      })

      fetchMock.post(`${API_URL}/renew`, {
        body: JSON.stringify({
          access_token: 'this-is-your-renewed-access_token-from_refresh'
        }),
        status: 201
      }, {
        overwriteRoutes: true
      })

      // Mocks the posts fetching request acting as an unauthenticated request
      fetchMock.get(`${API_URL}/posts`, {
        body: JSON.stringify({}),
        status: 401
      })

      tryRenewJwt = jest.spyOn(postsApi, '_tryRenewJwt')
    })

    afterAll(() => {
      fetchMock.reset()
      tryRenewJwt.mockRestore()
    })

    beforeEach(() => {
      RestfulClient.configure({
        tokenAttributeName: 'access_token',
        tokenRenewPath: `${API_URL}/renew`,
        withRefreshToken: true
      })

      sessionsApiPromise = sessionsApi.create()
    })

    afterEach(() => {
      RestfulClient.reset()
    })

    it('stores the initial access token in the localStorage', () => (
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:jwt')).toEqual(
          'this-is-your-initial-access_token'
        )
      ))
    ))

    it('stores the initial refresh token in the localStorage', () => (
      sessionsApiPromise.then(() => (
        expect(localStorage.getItem('restfulclient:refresh')).toEqual(
          'this-is-your-initial-refresh_token'
        )
      ))
    ))

    describe('when the token has expired', () => {
      it('should call the renew path', () => (
        sessionsApiPromise.then(() => {
          postsApiPromise = postsApi.all()

          return postsApiPromise.then(response => (
            expect(tryRenewJwt).toHaveBeenCalled()
          ))
        })
      ))

      it('should send the refresh token as `refresh_token` to renew it', () => (
        sessionsApiPromise.then(() => (
          postsApi.all().then(response => (
            expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/renew`, {
              body: JSON.stringify({
                refresh_token: 'this-is-your-initial-refresh_token'
              }),
              headers: {
                "Accept": "application/json",
                "Content-Type": "application/json"
              },
              method: "POST"
            })
          ))
        ))
      ))

      it('should have updated the stored token from the localStorage', () => (
        sessionsApiPromise.then(() => {
          postsApiPromise = postsApi.all()

          return postsApiPromise.then(response => (
            expect(localStorage.getItem('restfulclient:jwt')).toEqual(
              'this-is-your-renewed-access_token-from_refresh'
            )
          ))
        })
      ))
    })

    describe('when the access_token is missing but there is a refresh_token', () => {
      beforeEach(() => {
        sessionsApiPromise.then(() => (
          localStorage.removeItem('restfulclient:jwt')
        ))
      })

      it('should call the renew path', () => {
        postsApiPromise = postsApi.all()

        return postsApiPromise.then(response => (
          expect(tryRenewJwt).toHaveBeenCalled()
        ))
      })

      it('should send the refresh token as `refresh_token` to renew it', () => (
        postsApi.all().then(response => (
          expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/renew`, {
            body: JSON.stringify({
              refresh_token: 'this-is-your-initial-refresh_token'
            }),
            headers: {
              "Accept": "application/json",
              "Content-Type": "application/json"
            },
            method: "POST"
          })
        ))
      ))

      it('should have updated the stored token from the configured torage', () => {
        postsApiPromise = postsApi.all()

        return postsApiPromise.then(response => (
          expect(localStorage.getItem('restfulclient:jwt')).toEqual(
            'this-is-your-renewed-access_token-from_refresh'
          )
        ))
      })
    })
  })
})
