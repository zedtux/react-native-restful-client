import fetchMock from 'fetch-mock'

import RestfulClient from '..'

// ~~~~ Config ~~~~
const API_URL = 'https://api.domain.co/api'

const POSTS = [
  {
    id: 1,
    title: 'First post',
    body: 'This is the first post.'
  },
  {
    id: 2,
    title: 'A brand new way of communicating',
    body: 'In this post we will see that ...'
  }
]

// ~~~~ Global Variables ~~~~
let sessionsApi
let sessionsApiFetch
let sessionsApiPromise
let postsApi
let postsApiFetch
let postsApiPromise

describe('RestfulClient JWT', () => {
  beforeAll(() => {
    class SessionsApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'sessions' })
      }
    }
    sessionsApi = new SessionsApi()
    sessionsApiFetch = jest.spyOn(sessionsApi, '_fetch')

    // Get posts with JWT from session
    class PostsApi extends RestfulClient {
      constructor () {
        super(API_URL, { resource: 'posts' })
      }
    }
    postsApi = new PostsApi()
    postsApiFetch = jest.spyOn(postsApi, '_fetch')
  })

  afterAll(() => {
    sessionsApiFetch.mockRestore()
    postsApiFetch.mockRestore()
  })

  describe('creating a session', () => {
    describe("with the token in the root's response body", () => {
      beforeAll(() => {
        // Mocks the session creation request
        fetchMock.post(`${API_URL}/sessions`, {
          body: JSON.stringify({
            token: 'this-is-your-token'
          }),
          status: 201
        })
      })

      beforeEach(() => {
        sessionsApiPromise = sessionsApi.create()
      })

      afterEach(() => {
        RestfulClient.reset()
      })

      it('should returns a JWT', () => (
        sessionsApiPromise.then(response => {
          expect(response.status).toEqual(201)
          return response.json()
        }).then(json =>
          expect(json).toEqual(
            expect.objectContaining({
                token: 'this-is-your-token'
              }
            )
          )
        )
      ))

      it('stores the JWT in the localStorage', () => (
        sessionsApiPromise.then(() => (
          expect(localStorage.getItem('restfulclient:jwt')).toEqual(
            'this-is-your-token'
          )
        ))
      ))

      it('should call only one time the fetch function', () => (
        expect(sessionsApiFetch).toHaveBeenCalledTimes(1)
      ))

      it('should have sent a POST request', () => (
        expect(sessionsApiFetch).toHaveBeenCalledWith(`${API_URL}/sessions`, {
          body: JSON.stringify({}),
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
          },
          method: "POST"
        })
      ))

      describe('quering the protected Posts resource', () => {
        beforeAll(() => {
          // Mocks the session creation request
          fetchMock.get(`${API_URL}/posts`, {
            body: JSON.stringify(POSTS),
            status: 200
          })
        })

        afterAll(() => {
          fetchMock.reset()
        })

        it('should return existing posts', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return postsApiPromise.then(response => {
              expect(response.status).toEqual(200)
              return response.json()
            }).then(json =>
              expect(json).toEqual(expect.objectContaining(POSTS))
            )
          })
        ))

        it('should call only one time the fetch function', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(postsApiFetch).toHaveBeenCalledTimes(1)
          })
        ))

        it('should have sent a GET request with the JWT', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/posts`, {
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer this-is-your-token",
                "Content-Type": "application/json"
              },
              method: "GET"
            })
          })
        ))
      })
    })

    describe('with the token within response object and a custom token attribute', () => {
      beforeAll(() => {
        // Mocks the session creation request
        fetchMock.post(`${API_URL}/sessions`, {
          body: JSON.stringify({
            user: {
              access_token: 'this-is-your-user-access_token'
            }
          }),
          status: 201
        })
      })

      beforeEach(() => {
        RestfulClient.configure({
          requestCredentials: 'same-origin',
          tokenAttributeName: 'access_token',
          tokenParent: 'user'
        })

        sessionsApiPromise = sessionsApi.create()
      })

      afterEach(() => {
        RestfulClient.reset()
      })

      it('should respond with a token', () => (
        sessionsApiPromise.then(response => {
          expect(response.status).toEqual(201)
          return response.json()
        }).then(json => expect(json).toEqual(
            expect.objectContaining({
              user: {
                access_token: 'this-is-your-user-access_token'
              }
            })
          )
        )
      ))

      it('stores the token in the localStorage', () => (
        sessionsApiPromise.then(() => (
          expect(localStorage.getItem('restfulclient:jwt')).toEqual(
            'this-is-your-user-access_token'
          )
        ))
      ))

      it('should call only once the fetch function', () => (
        expect(sessionsApiFetch).toHaveBeenCalledTimes(1)
      ))

      it('should have sent a POST request', () => (
        expect(sessionsApiFetch).toHaveBeenCalledWith(`${API_URL}/sessions`, {
          credentials: 'same-origin',
          body: JSON.stringify({}),
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
          },
          method: "POST"
        })
      ))

      describe('quering the protected Posts resource', () => {
        beforeAll(() => {
          // Mocks the session creation request
          fetchMock.get(`${API_URL}/posts`, {
            body: JSON.stringify(POSTS),
            status: 200
          })
        })

        afterAll(() => {
          fetchMock.reset()
        })

        it('should return existing posts', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return postsApiPromise.then(response => {
              expect(response.status).toEqual(200)
              return response.json()
            }).then(json => expect(json).toEqual(
                expect.objectContaining(POSTS)
              )
            )
          })
        ))

        it('should call only one time the fetch function', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(postsApiFetch).toHaveBeenCalledTimes(1)
          })
        ))

        it('should have sent a GET request with the token', () => (
          sessionsApiPromise.then(() => {
            postsApiPromise = postsApi.all()

            return expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/posts`, {
              credentials: 'same-origin',
              headers: {
                "Accept": "application/json",
                "Authorization": "Bearer this-is-your-user-access_token",
                "Content-Type": "application/json"
              },
              method: "GET"
            })
          })
        ))
      })
    })
  })

  describe('without creating a session', () => {
    describe('quering the protected Posts resource', () => {
      beforeAll(() => {
        // Mocks the session creation request
        fetchMock.get(`${API_URL}/posts`, {
          body: JSON.stringify({}),
          status: 401
        })
      })

      afterAll(() => {
        fetchMock.reset()
      })

      beforeEach(() => {
        RestfulClient.reset()
        postsApiPromise = postsApi.all()
      })

      it('should return existing posts', () => (
        postsApiPromise.then(response => {
          expect(response.status).toEqual(401)
          return response.json()
        }).then(json => expect(json).toEqual(
            expect.objectContaining({})
          )
        )
      ))

      it('should call only one time the fetch function', () => (
        expect(postsApiFetch).toHaveBeenCalledTimes(1)
      ))

      it('should have sent a GET request without the token', () => (
        expect(postsApiFetch).toHaveBeenCalledWith(`${API_URL}/posts`, {
          headers: {
            "Accept": "application/json",
            "Content-Type": "application/json"
          },
          method: "GET"
        })
      ))
    })
  })
})
