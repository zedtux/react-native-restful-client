export default class RestfulClient {
  constructor (
    baseUrl = '', { resource = '', headers = {}, credentials = null } = {}
  ) {
    if (!baseUrl) throw new Error('missing baseUrl')
    if (!resource) throw new Error('missing resource')

    this.originalHeaders = {}

    if (headers === false) {
      this.headers = {}
    } else {
      this.resetHeaders()
      Object.assign(this.headers, headers, RestfulClient.globalHeaders || {})
      Object.assign(this.originalHeaders, this.headers, RestfulClient.globalHeaders || {})
    }

    this.baseUrl = baseUrl === '/' ? window.location.origin : baseUrl
    this.credentials = credentials
    this.resource = resource
  }

  /*
  ** Configuration
  */
  static configure (options = {}) {
    this.globalCredentials = options.requestCredentials || null
    this.globalHeaders = options.headers || null

    this.tokenAttributeName = options.tokenAttributeName || 'token'
    this.tokenParent = options.tokenParent

    // Token renew options
    this.tokenRenewPath = options.tokenRenewPath
    this.renewCallback = options.tokenRenewCallback
    this.configuredStorage = options.storage || 'local'
    this.withRefreshToken = options.withRefreshToken || false
  }

  static reset () {
    this.globalCredentials = null
    this.globalHeaders = null

    this.tokenAttributeName = 'token'
    this.tokenParent = null

    this.tokenRenewPath = null
    this.renewCallback = null
    this.configuredStorage = 'local'
    this.withRefreshToken = false

    this.clearToken()
  }

  /*
  ** Storage
  */
  static clearToken () {
    this.storage('accessToken').removeItem('restfulclient:jwt')
    this.storage('refreshToken').removeItem('restfulclient:refresh')
  }

  static jwt () {
    return this.storage('accessToken').getItem('restfulclient:jwt')
  }

  static refreshToken () {
    return this.storage('refreshToken').getItem('restfulclient:refresh')
  }

  static storage (type = null) {
    if (typeof this.configuredStorage === 'string') {
      return this.configuredStorage === 'session' ? sessionStorage : localStorage
    }

    if (typeof this.configuredStorage === 'object') {
      if (Object.hasOwn(this.configuredStorage, type)) {
        return this.configuredStorage[type] === 'session' ? sessionStorage : localStorage
      } else {
        return localStorage
      }
    }

    return localStorage
  }

  static _writeJwt (jwt) {
    this.storage('accessToken').setItem('restfulclient:jwt', jwt)
  }

  static _writeRefreshToken (jwt) {
    this.storage('refreshToken').setItem('restfulclient:refresh', jwt)
  }

  resetHeaders () {
    this.headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }

  /*
  ** Build the full route to be called.
  *
  *  Given baseUrl is 'https://mydom.co/api' and resource is 'users'
  *
  *  - Without any params: https://mydom.co/api/users
  *  - With id 1: https://mydom.co/api/users/1
  *  - With id 1 and path 'unlock': https://mydom.co/api/users/1/unlock
  */
  _fullRoute (id = null, path = null) {
    if (path === null && !id) throw new Error('missing id')
    return `${this.baseUrl}/${this.resource}${id ? `/${id}` : ''}${path && `/${path}`}`
  }

  _fetch(fullRoute, opts) {
    return fetch(fullRoute, opts)
  }

  _headersWithExecutedCallbacks() {
    return Object.keys(this.originalHeaders).reduce((accu, key) => {
      const current = this.originalHeaders[key]

      accu[key] = typeof current === 'function' ? current() : current

      return accu
    }, {})
  }

  /*
  ** Build and execute RESTful request.
  *
  *  method: HTTP Verb to be used for this request (GET, POST, PUT, PATCH, DELETE) (Required)
  *  id: Resource ID (Optional)
  *  path: Additional path for this request (See _fullRoute()) (Optional)
  *  body: Request body or request query when verb is GET or DELETE (Optional)
  */
  _buildFetch (method, { id = 0, path = '', body = null } = {}) {
    let fullRoute = this._fullRoute(id, path)
    if (['GET', 'DELETE'].includes(method) && body) {
      const qs = require('qs')
      const query = qs.stringify(body)
      fullRoute = `${fullRoute}?${query}`
      body = undefined
    }

    let opts = {
      method,
      headers: this.headers
    }

    if (body) { Object.assign(opts, { body: JSON.stringify(body) }) }

    const jwt = RestfulClient.jwt()
    if (jwt) {
      Object.assign(opts.headers, { 'Authorization': `Bearer ${jwt}` })
    } else {
      opts.headers = Object.assign({}, this._headersWithExecutedCallbacks())
    }

    if (this.credentials || RestfulClient.globalCredentials) {
      opts.credentials = this.credentials || RestfulClient.globalCredentials
    }

    return this._fetch(fullRoute, opts)
  }

  /*
  ** This function runs the fetch request, then checks for a JWT token from the
  *  response's body and stores it if any.
  *
  *  Later, when the fetch request is rejected (error 401), this function tries
  *  to renew the JWT, in the case of the renewPath is configured, and update
  *  it before to run again the fetch request with the new JWT.
  *  This should makes transparent to the external app the JWT renewal.
  *
  */
  _buildFetchWithJwtSupport (method, { id = 0, path = '', body = null } = {}) {
    const fetchPromise = this._buildFetch(method, {
      id: id, path: path, body: body
    })

    return fetchPromise.then(response => {
      if (response.ok) {
        /*
        ** Before to return the successful Promise, let's see if we don't have
        *  a JWT in the body ...
        *
        *  We are using .clone() here in order to do somehting with the data,
        *  without altering the response object which will be returned to the
        *  external app.
        */
        response.clone().text().then(
          (text) => (text && text !== '' ? JSON.parse(text) : {})
        ).then(
          (json) => this._storeTokenIfAnyFoundFrom(json)
        )

        // Returns the successful Promise to the external so that it can
        // consume its precious data.
        return fetchPromise
      } else {
        if (response.status === 401) {
          // In the case we don't have an access_token but we do have a
          // refresh_token, like when storing access_token in the sessionStorage
          // and the refresh_token in the localStorage
          const currentJwt = RestfulClient.jwt() || RestfulClient.refreshToken()
          if (currentJwt) {
            if (RestfulClient.tokenRenewPath) {
              return this._tryRenewJwt(currentJwt).then(
                response => response.json()
              ).then(json => {
                const newJwt = this._storeTokenIfAnyFoundFrom(json)

                // Triggers the given callback function if any has been given
                // from the setJwtRenewPath function
                if (newJwt && RestfulClient.renewCallback) {
                  RestfulClient.renewCallback(newJwt)
                }

                // Re-run the initial request, with the new JWT, and return the
                // promise, whatever happen ...
                return this._buildFetch(method, {
                  id: id, path: path, body: body
                })
              }).catch(error => {
                // Something went wrong while renewing the JWT, let's the external
                // app knows it.
                return fetchPromise
              })
            } else {
              // Okay so we don't know where to renew the JWT, so let's return
              // the failed promise to the external app.
              return fetchPromise
            }
          } else {
            // We aren't authorized here, and have no JWT, so returns the
            // promise to the external app so that it can handle it and do
            // something.
            return fetchPromise
          }
        } else {
          // There's another type of error than unauthorized, so we just return
          // the Promise to the external app.
          return fetchPromise
        }
      }
    })
  }

  // Retrieves the JWT token from the response body.
  // In the case a `tokenParent` has been passed to the constructor, it will be
  // used instead in order to look for the token field.
  _storeTokenIfAnyFoundFrom (payload) {
    let refreshToken = null

    // Default place is at the root
    let source = payload || {}

    // But in the case a `tokenParent` has been configured, we're using it
    if (RestfulClient.tokenParent && RestfulClient.tokenParent in payload) {
      source = payload[RestfulClient.tokenParent]
    }

    // to retrieve the (access) token
    const accessToken = source[RestfulClient.tokenAttributeName]

    // and when enabled the refresh token
    if (RestfulClient.withRefreshToken) {
      refreshToken = source.refresh_token
    }

    // Stores what has been found if any
    if (accessToken) RestfulClient._writeJwt(accessToken)
    if (refreshToken) RestfulClient._writeRefreshToken(refreshToken)

    return accessToken
  }

  _tryRenewJwt (jwt) {
    let body = JSON.stringify({ [RestfulClient.tokenAttributeName]: jwt })

    if (RestfulClient.withRefreshToken) {
      body = JSON.stringify({ refresh_token: RestfulClient.refreshToken() })
    }

    return this._fetch(RestfulClient.tokenRenewPath, {
      method: 'POST',
      headers: this.originalHeaders,
      body
    })
  }

  request (method, { id = 0, path = '', body = null } = {}) {
    return this._buildFetchWithJwtSupport(method, {
      id: id, path: path, body: body
    })
  }

  all () {
    return this.request('GET')
  }

  // GET can be used to get a given resources ID or to a singular resource
  get ({ id = 0, query = '' } = {}) {
    return this.request('GET', { id: id, body: query })
  }

  create (body = {}) {
    return this.request('POST', { body: body })
  }

  update (id, body = {}) {
    return this.request('PATCH', { id: id, body: body })
  }

  destroy (id) {
    return this.request('DELETE', { id: id })
  }
}

RestfulClient.tokenAttributeName = 'token'
RestfulClient.withRefreshToken = false
