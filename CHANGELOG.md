# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## [1.4.12] - 2023-07-14
### Changed
- Renew access_token when it is missing but a refresh_token is present

## [1.4.11] - 2023-07-03
### Added
- Support configure `headers` with callback function as value

## [1.4.10] - 2023-06-20
### Added
- Configure headers to define headers to be included in each requests

## [1.4.9] - 2023-06-07
### Changed
- Storage configuration allowing one to configure a different storage per token

## [1.4.8] - 2023-06-07
### Added
- Storage configuration to use localStorage or sessionStorage

## [1.4.7] - 2023-06-02
### Added
- Setting the fetch request `credentials` property per resources or globally using the `configure` function

## [1.4.6] - 2023-06-02
### Added
- Setting `headers: false` will send empty headers

## [1.4.5] - 2023-05-30
### Added
- Uses `window.location.origin` when passing a slash as the `baseUrl`

## [1.4.4] - 2023-04-27
### Changed
- Various code refactoring

## [1.4.3] - 2023-04-26
### Added
- `withRefreshToken` option

### Changed
- Upgrades node-fetch and fetch-mock

## [1.4.2] - 2023-04-25
### Fixed
- Uses the configured token name to renew the token

## [1.4.1] - 2023-04-21
### Added
- `clearToken` function to *only clear the stored token*, while the `reset`
  function clears the configuration *and* clear the stored token.

## [1.4.0] - 2023-04-20
### Added
- `configure` function to update all library options

## [1.3.3] - 2022-12-08
### Fixed
- Crash when fetch response is null

## [1.3.2] - 2022-09-06
### Added
- Callback function being triggered on JWT renew successful #5

## [1.3.1] - 2021-01-29
### Fixed
- Possible "Unexpected end of JSON input" error when response's body is empty like with 204 code responses

## [1.3.0] - 2020-10-12
### Added
- JWT automatic renewal

## [1.2.0] - 2020-10-12
### Added
- Stores the JWT within a localStorage `restfulclient:jwt` key
- `tokenParent` attribute to the `constructor` in order to set an object name from where to look for the `token` field

### Removed
- Reseting the token from the `constructor`

## [1.1.0] - 2019-06-24
### Fixes
- Allows getting resource with alphabetical ID
- Transfers the package to the Pharmony group

## [1.0.1] - 2018
### Added
- Automatic JWT #2

### Removed
- Delay simulation feature #4

## [1.0.0] - 2017
- Initial version

[Unreleased]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.12...master
[1.4.12]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.11...v1.4.12
[1.4.11]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.10...v1.4.11
[1.4.10]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.9...v1.4.10
[1.4.9]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.8...v1.4.9
[1.4.8]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.7...v1.4.8
[1.4.7]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.6...v1.4.7
[1.4.6]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.5...v1.4.6
[1.4.5]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.4...v1.4.5
[1.4.4]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.3...v1.4.4
[1.4.3]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.2...v1.4.3
[1.4.2]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.1...v1.4.2
[1.4.1]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.4.0...v1.4.1
[1.4.0]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.3.3...v1.4.0
[1.3.3]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.3.2...v1.3.3
[1.3.2]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.3.1...v1.3.2
[1.3.1]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.3.0...v1.3.1
[1.3.0]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.2.0...v1.3.0
[1.2.0]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.1.0...v1.2.0
[1.1.0]: https://gitlab.com/pharmony/restful-json-api-client/-/compare/v1.0.1...v1.1.0
[1.0.1]: https://gitlab.com/pharmony/restful-json-api-client/-/tags/v1.0.1
